This contains the settings for the country map on the sources page of D3.

Map geometries and country names are in `jvectormap-world-mill-en.js`. N.B. not all D3 countries are included in the map (only 171 out of 215).

Remember to bump the version number in `package.json` when merging new changes for them to take effect.